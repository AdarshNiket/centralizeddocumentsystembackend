package com.adarshniket.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.adarshniket.dto.EmpDetails;
import com.adarshniket.service.EmpDetailsService;

/**
 * @author AdarshNiket
 *
 */

@RestController
public class EmpDetailsController{
	
	@Autowired
//	private EmpDetailsService empdetailService;
	EmpDetailsService empdetailService;
	
//	@Autowired(required = true)
//	@Qualifier(value = "empdetailService")
//	public void setEmpDetailsService(EmpDetailsService empdetservice){
//		this.empdetailService = empdetservice;
//	}
	
	@RequestMapping(value = "/employees", method = RequestMethod.GET)
	public String listEmployees(Model model){
		model.addAttribute("employee", new EmpDetails());
		model.addAttribute("listEmployees", this.empdetailService.listEmp());
		return "employee";
	}
	
	// For adding and updating Employee Detail
	@RequestMapping(value = "/employee/add", method = RequestMethod.POST)
	public String addEmployee(@ModelAttribute("employee") EmpDetails empdet){
		if(empdet.getEmpID() == 0){
			//new Employee, add it
			this.empdetailService.addEmp(empdet);
		}
		else{
			//existing Employee, call update
			this.empdetailService.updateEmp(empdet);
		}
		return "redirect:/employees";
	}
	
	@RequestMapping("/employee/remove/{id}")
	public String removeEmployee(@PathVariable("id") int id){
		this.empdetailService.removeEmp(id);
		return "redirect:/employees";
	}
	
}