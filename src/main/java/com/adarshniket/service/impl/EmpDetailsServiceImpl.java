package com.adarshniket.service.impl;

import java.util.Collection;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.adarshniket.dao.EmpDetailsDAO;
import com.adarshniket.dto.EmpDetails;
import com.adarshniket.service.EmpDetailsService;

/**
 * @author AdarshNiket
 *
 */

@Service
public class EmpDetailsServiceImpl implements EmpDetailsService{
	private EmpDetailsDAO empdetailsDAO;
	public void setEmpdetailsDAO(EmpDetailsDAO empdetailsDAO) {
		this.empdetailsDAO = empdetailsDAO;
	}
	
	@Override
	@Transactional
	public void addEmp(EmpDetails user) {
		this.empdetailsDAO.addEmp(user);		
	}

	@Override
	@Transactional
	public Collection<EmpDetails> listEmp() {
		return this.empdetailsDAO.listEmp();
	}

	@Override
	@Transactional
	public void updateEmp(EmpDetails user) {
		this.empdetailsDAO.updateEmp(user);
	}

	@Override
	@Transactional
	public void removeEmp(int id) {
		this.empdetailsDAO.removeEmp(id);
	}
}