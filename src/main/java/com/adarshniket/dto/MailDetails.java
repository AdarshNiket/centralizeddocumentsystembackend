/**
 * 
 */

package com.adarshniket.dto;

import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * @author AdarshNiket
 *
 */

@Entity
public class MailDetails {
	
	@Id
	private int id;
	private String subject;
	private String body;
	private String fromTag;
	private String toTag;
	private String attachment;
	private String sentFlag;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getBody() {
		return body;
	}
	public void setBody(String body) {
		this.body = body;
	}
	public String getFromTag() {
		return fromTag;
	}
	public void setFromTag(String fromTag) {
		this.fromTag = fromTag;
	}
	public String getToTag() {
		return toTag;
	}
	public void setToTag(String toTag) {
		this.toTag = toTag;
	}
	public String getAttachment() {
		return attachment;
	}
	public void setAttachment(String attachment) {
		this.attachment = attachment;
	}
	public String getSentFlag() {
		return sentFlag;
	}
	public void setSentFlag(String sentFlag) {
		this.sentFlag = sentFlag;
	}
	
	

}
