/**
 * 
 */

package com.adarshniket.dto;

import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * @author AdarshNiket
 *
 */

@Entity
public class DocDetails {
	
	@Id
	private int id;
	private String name;
	private String path;
	private String DocType;
	private int empID;
	private String uploadFlag;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
	public String getDocType() {
		return DocType;
	}
	public void setDocType(String docType) {
		DocType = docType;
	}
	public int getEmpID() {
		return empID;
	}
	public void setEmpID(int empID) {
		this.empID = empID;
	}
	public String getUploadFlag() {
		return uploadFlag;
	}
	public void setUploadFlag(String uploadFlag) {
		this.uploadFlag = uploadFlag;
	}
	
	

}
