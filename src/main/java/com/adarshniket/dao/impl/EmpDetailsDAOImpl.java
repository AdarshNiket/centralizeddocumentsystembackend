package com.adarshniket.dao.impl;

import java.util.Collection;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adarshniket.dao.EmpDetailsDAO;
import com.adarshniket.dto.EmpDetails;

/**
 * @author AdarshNiket
 *
 */

public class EmpDetailsDAOImpl implements EmpDetailsDAO{
	
	private static final Logger logger = LoggerFactory.getLogger(EmpDetailsDAOImpl.class);
	private SessionFactory sessionFactory;
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Override
	public void addEmp(EmpDetails user) {
		Session session  = this.sessionFactory.getCurrentSession();
		session.persist(user);
		logger.info("User saved successfully, User Details = " + user);
	}

	@Override
	public Collection<EmpDetails> listEmp() {
		Session session  = this.sessionFactory.getCurrentSession();
		Collection<EmpDetails> empList = session.createQuery(" from emps_table").list();
		for(EmpDetails user : empList){
			logger.info("User List ::" + user);
		}
		return empList;
	}

	@Override
	public void updateEmp(EmpDetails user) {
		Session session  = this.sessionFactory.getCurrentSession();
		session.update(user);
		logger.info("User updated Successfully, User Details = " + user);
	}

	@Override
	public void removeEmp(int id) {
		Session session  = this.sessionFactory.getCurrentSession();
		EmpDetails user = session.load(EmpDetails.class, id);
		if(null != user){
			session.delete(user);
		}
		logger.info("User delete successfully, User Details = " + user);
	}
}