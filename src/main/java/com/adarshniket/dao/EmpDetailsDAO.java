package com.adarshniket.dao;

import java.util.Collection;

import com.adarshniket.dto.EmpDetails;

/**
 * @author AdarshNiket
 *
 */

//This is Interface class for EmpDetails/user table, which contains interface function declaration for CRUD operations. 
public interface EmpDetailsDAO {
	public void addEmp(EmpDetails user);
	public Collection<EmpDetails> listEmp();
	public void updateEmp(EmpDetails user);
	public void removeEmp(int id);
}
