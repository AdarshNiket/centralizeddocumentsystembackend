CREATE DATABASE `centralizedDocManager`;

USE `centralizedDocManager`;

DROP TABLE IF EXISTS `emps_table`;

CREATE TABLE `emps_table` (
`emp_id` BIGINT(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
`username` varchar(225) DEFAULT NULL,
`password` varchar(225) DEFAULT NULL,
`first_name` varchar(225) DEFAULT NULL,
`last_name` varchar(225) DEFAULT NULL,
`gender` varchar(225) DEFAULT NULL,
`user_type` varchar(225) DEFAULT NULL,
`location` varchar(225) DEFAULT NULL,
`email` varchar(225) DEFAULT NULL,
`status` varchar(225) DEFAULT NULL,
`password_hint` varchar(225) DEFAULT NULL
)ENGINE=INNODB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `docs`;

CREATE TABLE `docs` (
`id` BIGINT(20) DEFAULT NULL,
`name` varchar(225) DEFAULT NULL,
`path` varchar(225) DEFAULT NULL,
`doc_type` varchar(225) DEFAULT NULL,
`emp_id` BIGINT(20) DEFAULT NULL,
`upload_flag` varchar(10) DEFAULT NULL
)ENGINE=INNODB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `mails`;

CREATE TABLE `mails` (
`id` BIGINT(20) DEFAULT NULL,
`subject` varchar(225) DEFAULT NULL,
`body` varchar(225) DEFAULT NULL,
`from_tag` varchar(20) DEFAULT NULL,
`to_tag` varchar(20) DEFAULT NULL,
`attachment` varchar(225) DEFAULT NULL,
`sent_flag` varchar(10) DEFAULT NULL
)ENGINE=INNODB DEFAULT CHARSET=utf8;