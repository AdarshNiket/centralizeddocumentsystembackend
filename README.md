#Central Document Manager System Backend

#Tech Stack used
1. Spring MVC
2. Hibernate ORM
3. MYSQL database

#Steps to install and run Application
1. mvn clean install
2. mvn compile (optional)
3. mvn jetty:run