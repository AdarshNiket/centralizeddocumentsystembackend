/**
 * 
 */

package com.adarshniket.dto;


import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;


/**
 * @author AdarshNiket
 *
 */

@Entity (name="emps_table")
public class EmpDetails {
	
	@Id @GeneratedValue
	private int empID;
//	@Column (name="username")
	private String userName;
	private String password;
	private String firstName;
	private String lastName;
	private String gender;
	private String userType;
	private String location;
	private String email;
	private String status;
	private String passwordHint;
	@OneToOne
	private MailDetails mail;
	@OneToMany
	private Collection<DocDetails> doc = new ArrayList<DocDetails>();
	
	public MailDetails getMail() {
		return mail;
	}
	public void setMail(MailDetails mail) {
		this.mail = mail;
	}
	public Collection<DocDetails> getDoc() {
		return doc;
	}
	public void setDoc(Collection<DocDetails> doc) {
		this.doc = doc;
	}
	
	public int getEmpID() {
		return empID;
	}
	public void setEmpID(int empID) {
		this.empID = empID;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getUserType() {
		return userType;
	}
	public void setUserType(String userType) {
		this.userType = userType;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getPasswordHint() {
		return passwordHint;
	}
	public void setPasswordHint(String passwordHint) {
		this.passwordHint = passwordHint;
	}
	
}
