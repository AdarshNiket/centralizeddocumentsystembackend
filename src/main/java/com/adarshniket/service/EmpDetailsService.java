package com.adarshniket.service;

import java.util.Collection;

import com.adarshniket.dto.EmpDetails;

/**
 * @author AdarshNiket
 *
 */

public interface EmpDetailsService {
	public void addEmp(EmpDetails user);
	public Collection<EmpDetails> listEmp();
	public void updateEmp(EmpDetails user);
	public void removeEmp(int id);
}
