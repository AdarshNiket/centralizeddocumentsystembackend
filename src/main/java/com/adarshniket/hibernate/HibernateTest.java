/**
 * 
 */

package com.adarshniket.hibernate;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import com.adarshniket.dto.EmpDetails;

/**
 * @author AdarshNiket
 *
 */

public class HibernateTest {

	/**
	 * @param args
	 */
	
	public static void main(String[] args) {
		EmpDetails emp = new EmpDetails();
		emp.setFirstName("Nikhil");
		emp.setLastName("A.M.");
		
		SessionFactory empfactory = new Configuration().configure().buildSessionFactory();
		Session session = empfactory.openSession();
		try{
			session.beginTransaction();
			session.save(emp);
		}
		catch(SecurityException e){ // Double check Exception used here. Using random Exception to run the code. //
			session.getTransaction().rollback();
		}
		session.getTransaction().commit();
		session.close();
		
		/*Object user = null;
		session = empfactory.openSession();

		session.beginTransaction();
		user = session.get(EmpDetails.class,1);
		System.out.println("User Name is " + ((EmpDetails) user).getFirstName());	*/	
	}

}
